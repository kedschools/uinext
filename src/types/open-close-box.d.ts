namespace JSX {
	interface IntrinsicElements {
	  'open-close-box': OpenCloseBoxAttributes;
	}
  
	interface OpenCloseBoxAttributes {
	  id?: string;
	  open?: boolean;
	  dir?: 'ltr' | 'rt';
	  primary?: string;
	  secondary?: string;
	  warning?: string;
	  info?: string;
	  success?: string;
	  children: Element[];
	}
  }
  