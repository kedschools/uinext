namespace JSX {
	interface IntrinsicElements {
	  'task-box': TaskBoxAttributes;
	}
  
	interface TaskBoxAttributes {
	  id?: string;
	  type?: 'add' | 'progress';
	  value?: number;
	  added?: boolean;
	  completed?: boolean;
	  children: Element[];
	}
  }
  