import React from 'react';

export interface InputProps extends Omit<React.InputHTMLAttributes<HTMLInputElement>, "size"> {
  title: string;
  size?: number | 'large' | 'small';
  onChange?: (value:any )=> void;
  submitOnEnter?: (value:any )=> void;
}
export interface TextInputProps extends InputProps {}

export const TextInput = ({
  title,
  disabled,
  size,
  onChange,
  submitOnEnter,
  ...props
}: TextInputProps) => {
  const classes = ['kui-input', disabled ? 'disabled' : undefined, size]
    .filter(Boolean)
    .join(' ');

  return (
    <label className={classes}>
      <input
        type="text"
        required={true}
        onChange={(ev) => onChange && onChange(ev.target.value)}
        {...props}
        disabled={disabled}
        onKeyUp={(e) => {
          if (!submitOnEnter) return;
          if (e.code === 'Enter') {
            e.preventDefault();
            e.stopPropagation();
            submitOnEnter && submitOnEnter(e.currentTarget.value);
          }
        }}
      />
      <span>{title}</span>
    </label>
  );
};

export interface SimpleSelectProps  extends Omit<React.SelectHTMLAttributes<HTMLSelectElement>, "size"> {
  title: string;
  disabled?: boolean;
  value?: string;
  size?: number | 'large' | 'small';
  options: { value: string; name?: string, disabled?: boolean }[];
}

export const SimpleSelect = ({
  title,
  disabled,
  size,
  options,
  style,
  ...props
}: SimpleSelectProps) => {
  const classes = ['kui-input', disabled ? 'disabled' : undefined, size]
    .filter(Boolean)
    .join(' ');

  return (
    <label className={classes} style={style}>
      <select {...props}>
        {options.map(({ value, name, disabled }, i) => (
          <option value={value} key={i} disabled={disabled}>
            {name ?? value}
          </option>
        ))}
      </select>
      <span>{title}</span>
    </label>
  );
};
