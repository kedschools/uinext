import React, { createRef, ReactNode, useEffect } from 'react';
import { Button } from '.';
import { DialogOpts } from '../grouping/Dialog';

export interface PromptButton {
  title: string;
  action?: (ev: Event) => void;
  isDefault?: boolean;
}
export interface PromptProps {
  options?: Partial<DialogOpts>;
  size?: 'small' | 'large';
  className?: string;
  children: ReactNode;
  buttons?: PromptButton[];
}

export const Prompt = ({ className, size, children, buttons }: PromptProps) => {
  let promptButtons: PromptButton[] = buttons ?? [
    { title: 'Stäng', isDefault: true },
  ];
  const defaultButton = promptButtons.find(
    (btn) => btn.isDefault
  ) as PromptButton;
  promptButtons = [
    ...promptButtons.filter((btn) => !btn.isDefault),
    defaultButton,
  ];

  const butttons = (
    <>
      {promptButtons.map((btn) => (
        <Button
          action={btn.action}
          classNames={btn.isDefault ? ['primary'] : []}
        >
          {btn.title}
        </Button>
      ))}
    </>
  );
  return (
    <Modal size={size} className={className} buttons={buttons}>
      {children}
    </Modal>
  );
};

export interface SimplePromptProps {
  msg: string;
  onOk: (ev: Event) => void;
  okLabel?: string;
  onCancel?: (ev: Event) => void;
  cancelLabel?: string;
}

export const SimplePrompt = ({
  msg,
  onCancel,
  onOk,
  cancelLabel,
  okLabel,
}: SimplePromptProps) => {
  const promptButtons: PromptButton[] = [
    { title: cancelLabel ?? 'Avbryt', action: onCancel },
    { title: okLabel ?? 'Ok', action: onOk, isDefault: true },
  ];
  const body = <p>{msg}</p>;
  const buttons = (
    <>
      {promptButtons.map((btn) => (
        <Button
          action={btn.action}
          classNames={btn.isDefault ? ['primary'] : []}
        >
          {btn.title}
        </Button>
      ))}
    </>
  );

  return <Modal buttons={buttons}>{body}</Modal>;
};

export interface ModalProps extends React.BaseHTMLAttributes<HTMLElement> {
  size?: 'small' | 'large';
  buttons?: ReactNode;
  suppliesSection?: boolean;
}
export const Modal = ({
  className,
  size,
  children,
  buttons,
  suppliesSection,
  style,
  ...props
}: ModalProps) => {
  const dialogRef = createRef<HTMLDivElement>();
  const classNames = ['prompt', className, size].filter(Boolean).join(' ');

  // Handle keyboard eventst
  const handleInput = (ev: KeyboardEvent) => {
    if (ev.key === 'Escape') {
      ev.stopPropagation();
      ev.preventDefault();

      (
        dialogRef.current?.querySelector(
          '.header .controls .kui-btn.warning.btn-action'
        ) as HTMLButtonElement
      ).click();
    }
    if (ev.key === 'Enter') {
      const target = ev.target as HTMLElement;
      if (target.isContentEditable || target.nodeName === 'TEXTAREA') return;

      ev.stopPropagation();
      ev.preventDefault();

      (
        dialogRef.current?.querySelector(
          '.kui-btn-group .kui-btn[default]'
        ) as HTMLButtonElement
      ).click();
    }
  };

  useEffect(() => {
    const localRef = dialogRef.current;
    // Focus element to capture input events
    localRef?.focus({ preventScroll: true });
    // Prevent scrolling of page body
    document.body.style.overflow = 'hidden';
    document.addEventListener('keyup', handleInput);

    return () => {
      if (
        !document.querySelector('.kui-dialog') ||
        document.querySelector('.kui-dialog') === localRef
      ) {
        document.body.style.overflow = 'auto';
      }

      document.removeEventListener('keyup', handleInput);
    };
  }, [dialogRef]);

  return (
    <div className="kui-dialog" ref={dialogRef}>
      <div className="backdrop"></div>
      <div className={classNames} style={style}>
        {suppliesSection ? (
          children
        ) : (
          <>
            <section className="body" {...props}>
              {children}
            </section>
            <div className="kui-btn-group">{buttons}</div>
          </>
        )}
      </div>
    </div>
  );
};
