import React, { CSSProperties } from 'react';

type MixedType = Omit<
  React.HTMLProps<
    HTMLAnchorElement & Pick<HTMLButtonElement, 'disabled' | 'name' | 'value'>
  >,
  'action' | 'size'
>;

export interface ButtonProps extends MixedType {
  /**
   * Array of classnames to add to the Button
   */
  classNames?: string[];
  /**
   * Fontawesome icon to add to the Button
   */
  faIcon?: string;
  /**
   * Type of button to show
   */
  type?: 'button' | 'badge' | 'icon' | 'link' | 'action';
  /**
   * What is the context for the buttonn
   */
  context?:
    | 'primary'
    | 'secondary'
    | 'success'
    | 'danger'
    | 'warning'
    | 'info'
    | 'default'
    | 'light'
    | 'dark';
  /**
   * Should this be in another size?
   */
  size?: 'large' | 'small';
  isDefault?: boolean;
  disabled?: boolean;
  action?: (ev: MouseEvent) => void;
  url?: string;
  style?: CSSProperties;
  children?: React.ReactNode;
}

export const Button: React.FC<ButtonProps> = ({
  classNames,
  faIcon,
  type = 'button',
  context,
  size,
  disabled,
  isDefault = false,
  action,
  url,
  children,
  ...rest
}) => {
  // Creeate tag depending on content
  const Tag = `${url || rest.href || (type === 'link' || type === 'action') ? 'a' : 'button'}`;

  const className = [
    'kui-btn',
    ...(classNames ?? []),
    size? `btn-${size}` : null,
    rest.className,
    disabled && Tag === 'a' ? 'btn-disabled' : null,
    context,
    isDefault? 'btn-default' : null,
    ...(type && type !== 'button' ? [`btn-${type}`, 'animate-scale'] : [])
  ].filter(Boolean).join(' ');

  // Clear router-props
  const target = url || rest.href ? rest.target ?? '_blank' : undefined;

  // Apply our changes to props
  const newProps: ButtonProps = {
    tabIndex: 0,
    ...rest,
    className,
    onClick: action as any,
    target,
  };
  if (disabled && Tag === 'button') newProps.disabled = disabled;
  if (Tag === 'a' && !newProps.href) newProps.href = url;

  // Remove our deprecated props and props specific for router
  ['addClass', 'label', 'history', 'loction', 'match', 'staticContext'].forEach(
    (key) => {
      delete (newProps as any)[key];
    }
  );

  return (
    <Tag {...newProps}>
      {faIcon && (
        <i
          className={faIcon}
          style={type ? { marginRight: 0 } : undefined}
          aria-hidden="true"
        ></i>
      )}

      {children}
    </Tag>
  );
};

export const ButtonGroup: React.FC<React.HTMLProps<HTMLDivElement>> = ({ children, ...rest }) => {
  rest.className = `${rest.className?? ''} kui-btn-group`;
  return <div {...rest}>{children}</div>
}
