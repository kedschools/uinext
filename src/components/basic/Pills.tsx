import React, { ReactNode } from 'react';

export const Pill: React.FC<React.HTMLProps<HTMLSpanElement>> = (props) => {
  const classes = ['kui-pill', props.className ?? ''].filter(Boolean).join(' ');
  const newProps = { ...props, className: classes };
  delete newProps.children;
  return <span {...newProps}>{props.children}</span>;
};

export const PillGroup: React.FC<React.HTMLProps<HTMLSpanElement>> = (
  props
) => {
  const classes = ['kui-pill-list', props.className ?? '']
    .filter(Boolean)
    .join(' ');
  const newProps = { ...props, className: classes };
  delete newProps.children;
  return <span {...newProps}>{props.children}</span>;
};
