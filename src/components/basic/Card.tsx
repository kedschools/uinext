import React, { ReactNode } from 'react';

export const Card: React.FC<React.HTMLProps<HTMLDivElement>> = (props) => {
  const classes = ['kui-card', props.className?? ''].filter(Boolean).join(' ');
  const newProps = {...props, className: classes};
  delete newProps.children;
  return <div {...newProps}>{props.children}</div>;
};
