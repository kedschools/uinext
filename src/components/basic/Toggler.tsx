import React from 'react';

export interface Props {
  onToggle: (value: string) => void;
  colorOff?: string;
  colorOn?: string;
  labelOff?: string;
  labelOn?: string;
  isActive?: boolean;
  disabled?: boolean;
  innerLabel?: boolean;
  size?: 'large' | 'small';
  id?: string;
}

export const Toggler = ({
  onToggle,
  colorOff,
  colorOn,
  labelOff,
  labelOn,
  isActive,
  disabled,
  innerLabel,
  size,
  id,
}: Props) => {
  const tSize = !(labelOff && labelOn) ? 'small' : size ? size : undefined;
  const classes = [
    'kui-toggler',
    disabled ? 'disabled' : undefined,
    innerLabel ? 'inner-label' : undefined,
    tSize,
  ]
    .filter(Boolean)
    .join(' ');

  const styles: { [id: string]: string } = {};
  if (colorOff) styles['--clr-Off'] = colorOff;
  if (colorOn) styles['--clr-On'] = colorOn;

  const togglerId = id ? id : labelOn + '-' + labelOff;

  return (
    <div className={classes} style={styles}>
      <input
        type="checkbox"
        defaultChecked={isActive}
        disabled={disabled}
        onChange={(ev) => {
          ev.stopPropagation();
          if (!disabled) onToggle(ev.currentTarget.value);
        }}
        id={togglerId}
      />
      {innerLabel ? (
        <div className="toggler-control">
          <label className="inner-toggle" htmlFor={togglerId} />
          <span className="label label-on">{labelOn}</span>
          <span className="label label-off">{labelOff}</span>
          <span className="handle" />
        </div>
      ) : (
        <>
          <div className="toggler-control">
            <label className="inner-toggle" htmlFor={togglerId} />
            <span className="handle" />
          </div>
          <label className="inner-toggle" htmlFor={togglerId}>
            <span className="label label-on">{labelOn ?? ' '}</span>
            <span className="label label-off">{labelOff ?? ' '}</span>
          </label>
        </>
      )}
    </div>
  );
};
