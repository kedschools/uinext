import React, { ReactNode } from 'react';

export interface Props {
  children?: ReactNode;
  className?: string;
}

export const Box: React.FC = ({ children, className }: Props) => {
  const classes = ['kui-box', className].filter(Boolean).join(' ');
  return <div className={classes}>{children}</div>;
};
