
import React, { ReactNode } from 'react';

export const Area: React.FC<React.HTMLProps<HTMLDivElement>> = ({ children, className, style }) => {
  const classes = ['kui-area', className].filter(Boolean).join(' ');
  return <div className={classes} style={style}>{children}</div>;
};
