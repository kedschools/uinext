import React from 'react';

interface Props {
  state: string;
  title?: string;
  color?: string;
  children: React.ReactNode;
  disabled?: boolean;
  onChange?: () => void;
}

export const UIAddbox: React.FC<Props> = ({
  state,
  children,
  color,
  disabled,
  title,
  onChange = () => null,
}) => {
  return (
    <label
      className={
        (disabled ? 'disabled ' : '') + (color ? color + ' ' : '') + 'kui-addbox'
      }
    >
      {children}
      <input
        type="checkbox"
        checked={state === 'checked'}
        disabled={disabled}
        onChange={onChange}
      />
      <span
        className="custom-element"
        onKeyDown={(ev) => {
          if (ev.key === 'Enter' || ev.key === ' ') {
            ev.stopPropagation();
            ev.preventDefault();
            (ev.target as HTMLInputElement).click();
          }
        }}
        tabIndex={0}
        title={title ?? ''}
      ></span>
    </label>
  );
};
