import React, {
  createRef,
  ReactNode,
  useEffect,
  MouseEvent,
  KeyboardEvent,
} from 'react';
import { Button } from '../basic';

export interface DialogOpts {
  closeOnOutsideClick: boolean; // close dialog if clicking outside
  closeOnEscape: boolean; // close dialog if pressing escape
  customCloseButton: string | false; // ID of custom close buton
  submitButton: string | false; // ID of submit button
  showCloseButton: boolean; // show close button
}
export interface Props {
  size?: 'large';
  options?: Partial<DialogOpts>;
  onClose: () => void;
  className?: string;
  title?: string;
  children: ReactNode;
}

export const Dialog = ({
  size,
  className,
  title,
  children,
  onClose,
  options,
}: Props) => {
  // Apply options defaults
  const opts = Object.assign(
    {
      closeOnOutsideClick: true,
      closeOnEscape: true,
      customCloseButton: false,
      submitButton: false,
      showCloseButton: true,
    },
    options
  );

  const dialogRef = createRef<HTMLDivElement>();

  const classNames = ['kui-dialog', className, size ? size : null]
    .filter(Boolean)
    .join(' ');

  // Close dialog after foucsing parent dialog if it exists
  // so events on that dialog work
  const closeDialog = () => {
    (
      (dialogRef.current?.parentElement as HTMLElement)?.closest(
        '.kui-dialog'
      ) as HTMLElement
    )?.focus({ preventScroll: true });
    if (document.querySelector('.kui-dialog') === dialogRef.current) {
      document.body.style.overflow = 'auto';
    }
    onClose();
  };

  // Handle keyboard eventst
  const handleInput = (ev: KeyboardEvent) => {
    if (opts.closeOnEscape) {
      if (ev.key === 'Escape') {
        ev.stopPropagation();
        ev.preventDefault();
        closeDialog();
      }
    }
    if (opts.submitButton) {
      if (ev.key === 'Enter') {
        const target = ev.target as HTMLElement;
        if (target.isContentEditable || target.nodeName === 'TEXTAREA') return;

        ev.stopPropagation();
        ev.preventDefault();
        const btn = dialogRef.current?.querySelector(opts.submitButton) as any;
        btn.click();
      }
    }
  };

  // Handle click events
  const handleOutsideClick = (ev: MouseEvent) => {
    if (opts.closeOnOutsideClick) {
      ev.preventDefault();
      ev.stopPropagation();
      closeDialog();
    }
  };

  useEffect(() => {
    const localRef = dialogRef.current;
    // Focus element to capture input events
    localRef?.focus({ preventScroll: true });
    // Prevent scrolling of page body
    document.body.style.overflow = 'hidden';

    // If we have a custom close button fire our close-function
    // on click
    if (opts.customCloseButton) {
      const btn = localRef?.querySelector(opts.customCloseButton) as any;
      btn.addEventListener('click', () => closeDialog());
    }
    return () => {
      if (opts.customCloseButton) {
        const btn = localRef?.querySelector(opts.customCloseButton) as any;
        btn?.removeEventListener('click', () => closeDialog());
      }
    };
  }, [dialogRef, closeDialog, opts.customCloseButton]);

  return (
    <div
      className={classNames}
      ref={dialogRef}
      onKeyDown={handleInput}
      tabIndex={0}
    >
      <div className="backdrop" onClick={handleOutsideClick}></div>
      <div className="dialog">
        <section className="header">
          <div className="title">{title}</div>
          <div className="controls">
            {opts.showCloseButton && (
              <Button
                type="action"
                context="warning"
                faIcon="fas fa-times"
                action={closeDialog}
              ></Button>
            )}
          </div>
        </section>
        <section className="body">{children}</section>
        <section className="footer">
          <div className="kui-btn-group"> {/** handle this by config somehow */}
            <Button context="warning" action={closeDialog}>
              Avbryt
            </Button>
            <Button context="primary" action={closeDialog}>
              Spara
            </Button>
          </div>
        </section>
      </div>
    </div>
  );
};
