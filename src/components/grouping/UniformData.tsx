import React from 'react';

export interface Props {
  children: React.ReactElement;
}

export const UniformData = ({ children }: Props) => {
  return <div>{children}</div>;
};

export const UniformDataRow = ({}) => {
  return <div></div>;
};
