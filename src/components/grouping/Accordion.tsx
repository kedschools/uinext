import React, { ReactNode } from 'react';
export interface Props {
  single?: boolean;
  size?: 'large' | 'small';
  className?: string;
  closeLabel?: string;
  sections: Array<{ title: string; content: ReactNode }>;
}

export const Accordion = ({ single, size, className, closeLabel, sections }: Props) => {
  const classNames = [
    'kui-accordion',
    className,
    size? size : null
  ].filter(c => c).join(' ');

  return (
    <div className={classNames}>
      {sections.map(({ title, content }, idx) => (
        <section className="kui-open-close">
          <input
            type={single ? 'radio' : 'checkbox'}
            id={`ui-sec-${idx}`}
            name="kui-sec"
          />
          <label htmlFor={`ui-sec-${idx}`}>{title}</label>
          <div>{content}</div>
        </section>
      ))}
      {single && (
        <section className="kui-open-close">
          <input type="radio" id="kui-sec-x" name="kui-sec" />
          <label htmlFor="kui-sec-x" className="close">
            {closeLabel?? 'Close others'}
          </label>
        </section>
      )}
    </div>
  );
};
