import React, { ReactNode, useMemo } from 'react';

export interface Props {
  size?: 'large' | 'small';
  className?: string;
  title: string;
  children: ReactNode;
  extraControls?: ReactNode;
  extraTitle?: ReactNode;
  defaultOpen?: boolean;
  onOpenClose?: (becameOpen:boolean) => void;
}

export const OpenCloseBox = ({
  size,
  title,
  children,
  className,
  extraControls,
  defaultOpen,
  onOpenClose,
  extraTitle,
}: Props) => {
  const classNames = ['kui-open-close', className, size ?? null]
    .filter((c) => c)
    .join(' ');

  const idx = useMemo(() => Array.from(crypto.getRandomValues(new Uint8Array(5)))
    .map((a) => String.fromCharCode(a))
    .join(''), []);
  return (
    <section className={classNames}>
      <input
        type="checkbox"
        id={`ui-sec-${idx}`}
        name={`ui-sec-${idx}`}
        defaultChecked={defaultOpen}
        onChange={(ev) => onOpenClose && onOpenClose(ev.target.checked)}
      />
      <label htmlFor={`ui-sec-${idx}`}>
        <span className="title">{title}</span>
        {extraTitle && <span className="extra-title">{extraTitle}</span>}
        {extraControls && <span className="extra-controls">{extraControls}</span>}
      </label>
      <div>{children}</div>
    </section>
  );
};
