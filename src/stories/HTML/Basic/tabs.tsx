import { Area } from '../../../components/basic';

export const Tabs = () => (
  <Area className="info">
    <div className="justify-edge">
      <nav className="kui-tabs">
        <a className="tab active fixed">Upptakt</a>
        <a className="tab fixed">Arbetsgång</a>
        <a className="tab">Rättsfilosofi och etik</a>
        <a className="tab">Associationsrätt</a>
        <a className="tab">Avtals- och hyresrätt</a>
        <a className="tab">Annan rät</a>
        <a className="tab">En supergod huvudrätt</a>
        <a className="tab">Din rätt</a>
      </nav>
      <nav className="kui-tabs">
        <a className="tab fixed">
          <i className="fas fa-pencil-alt" aria-hidden="true"></i> Redigera
        </a>
      </nav>
    </div>
  </Area>
);
