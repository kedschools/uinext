import { Area } from '../../../components/basic';

export const Pills = () => (
  <>
  <Area>
  <p>Pills</p>
    <span className="kui-pill secondary">
      <i className="fas fa-building" />A town
    </span>
    <span className="kui-pill">
      <i className="fas fa-road" />A road
    </span>
  </Area>
  <Area>
  <p>Pill list</p>
  <div className="kui-pill-list">
    <span className="kui-pill">
      <i className="fas fa-building" />A town
    </span>
    <span className="kui-pill info">
      <i className="fas fa-road" />A road
    </span>

    <span className="kui-pill warning">
      <i className="fas fa-road" />A tree
    </span>
    </div>
  </Area>
  </>
);
