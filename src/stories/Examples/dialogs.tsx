import React, { ReactNode, createRef, useState } from 'react';
import { ButtonGroup, Card } from '../../components/basic';

import { Button } from '../../components/basic';
import { Dialog } from '../../components/grouping';

export const DialogsExample = () => {
  const [showDialog, setShowDialog] = useState(false);
  const [isLarge, setIsLarge] = useState(false);

  return (
    <Card>
      <p>Click on the button to open the dialog</p>
      <ButtonGroup>
        <Button
          context="warning"
          action={() => {
            setShowDialog(true);
          }}
        >
          Open small
        </Button>
        <Button
          context="info"
          action={() => {
            setIsLarge(true);
            setShowDialog(true);
          }}
        >
          Open large
        </Button>
      </ButtonGroup>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio fuga
        magni nam sit, nulla quisquam quaerat nisi, explicabo eligendi
        aspernatur nobis deleniti laborum animi! Deserunt fugiat sit illo vitae
        laudantium?
      </p>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore harum
        atque incidunt aliquam, hic ea a nisi quaerat, quam, officia sequi
        soluta. Est corrupti, debitis necessitatibus adipisci delectus nisi
        tempora.
      </p>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti
        corrupti nemo ratione dolorum iste tempora iure! Distinctio esse, nisi
        ipsum, soluta earum adipisci, velit rem aliquid mollitia labore illum.
        Aut.
      </p>
      <p>
        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Possimus
        libero commodi modi quidem nulla architecto qui doloribus enim magni
        explicabo itaque eaque, magnam fugit harum, odio debitis delectus
        deleniti dignissimos ea dolorem accusamus optio voluptatum? Beatae quos
        similique cupiditate amet fugiat ipsam necessitatibus est perferendis
        illum voluptates, rem facilis aspernatur sit dolorum earum explicabo
        doloribus eum. Asperiores molestiae voluptatibus neque possimus dolor
        molestias eius nulla nobis mollitia dignissimos expedita illo voluptas
        quae quisquam optio fugit natus laboriosam assumenda soluta dolores,
        porro at maxime id itaque. Est accusamus temporibus nulla labore
        voluptates reiciendis impedit hic, deserunt cumque laboriosam
        repellendus autem fuga.
      </p>
      {showDialog && (
        <Dialog
          onClose={() => {
            setShowDialog(false);
            setIsLarge(false);
          }}
          title="Min titel"
          size={isLarge ? 'large' : undefined}
        >
          <InnerDialog />
        </Dialog>
      )}
    </Card>
  );
};

const InnerDialog = () => {
  const [showChild, setShowChild] = useState(false);
  return (
    <>
      <p>Lite text</p>
      <Button context="danger" action={() => setShowChild(true)}>
        Open child
      </Button>
      {showChild && (
        <Dialog
          title="Sub dialog"
          options={{
            closeOnOutsideClick: false,
            showCloseButton: false,
            customCloseButton: '#closeBtnId',
            submitButton: '#submitBtnId',
          }}
          onClose={() => {
            setShowChild(false);
          }}
        >
          <p>Child dialog</p>
          <Button
            context="warning"
            id="closeBtnId"
            action={() => setShowChild(false)}
          >
            Close
          </Button>
          <Button
            context="success"
            id="submitBtnId"
            action={() => {
              console.log('submit');
              setShowChild(false);
            }}
          >
            Default action
          </Button>
        </Dialog>
      )}
    </>
  );
};
