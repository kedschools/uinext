export const Colors = () => {
  return (
  <div className="container">
        <div className="kui-card">
          <h1>Default</h1>
          <code>var(--clr-bg)</code>
          <pre className={'replace--backgroundColor'}>hsl(0, 0%, 93%)</pre>
        </div>

        <div className="kui-card primary">
          <h1>Primary</h1>
          <code>var(--clr-primary)</code>
          <pre className={'replace--backgroundColor'}>hsl(128, 12%, 38%)</pre>
        </div>

        <div className="kui-card secondary">
          <h1>Secondary</h1>
          <code>var(--clr-secondary)</code>
          <pre className={'replace--backgroundColor'}>hsl(29, 100%, 16%)</pre>
        </div>
        <div className="kui-card success">
          <h1>Success</h1>
          <code>var(--clr-success)</code>
          <pre className={'replace--backgroundColor'}>--clr-primary</pre>
        </div>
        <div className="kui-card info">
          <h1>Info</h1>
          <code>var(--clr-info)</code>
          <pre className={'replace--backgroundColor'}>hsl(240, 100%, 13%)</pre>
        </div>

        <div className="kui-card warning">
          <h1>Warning</h1>
          <code>var(--clr-warning)</code>
          <pre className={'replace--backgroundColor'}>hsl(0, 100%, 16%)</pre>
        </div>
      </div>
  );
};
