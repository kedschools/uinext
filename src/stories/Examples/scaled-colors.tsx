export const ScaledColors = () => {
  const colors: string[] = Array.from(document.styleSheets)
    .filter(
      (sheet) =>
        sheet.href === null || sheet.href.startsWith(window.location.origin)
    )
    .map((css) =>
      Array.from(css.cssRules).filter(
        (css) => (css as any).selectorText === ':root'
      )
    )
    .flat()
    .map(
      (css) =>
        Array.from((css as any).style).filter(
          (css) =>
            (css as string).startsWith('--clr') &&
            (css as string).endsWith('00')
        ) as string[]
    )
    .flat()
    .filter(css => !css.includes('-shade-'))
    .sort();
    
  return (
    <div className="container">
      <ul>
        {colors.map((color) => (
          <li style={{ backgroundColor: `var(${color})` }}>{color}</li>
        ))}
      </ul><ul style={{color: 'white'}}>
        {colors.map((color) => (
          <li style={{ backgroundColor: `var(${color})` }}>{color}</li>
        ))}
      </ul>
    </div>
  );
};
