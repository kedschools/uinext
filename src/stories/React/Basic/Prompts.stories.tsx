import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import { Prompt, PromptProps } from '../../../components/basic/Prompt';

export default {
  title: 'React/Basic/Prompt',
  component: Prompt,
} as Meta;

const Template: Story<PromptProps> = (args) => <Prompt {...args} />;

export const Default = Template.bind({});
Default.args = {
  children: 'I said close me!',
  buttons: [
    {
      title: 'Close',
      action: (ev) => {
        console.log('clicked close');
      },
      isDefault: true,
    },
    { title: 'Cancel', },
    { title: 'Do nothing', },
  ],
};
