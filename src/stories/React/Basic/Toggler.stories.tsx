import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import { Toggler, Props } from '../../../components/basic/Toggler';

export default {
  title: 'React/Basic/Toggler',
  component: Toggler,
} as Meta;

const Template: Story<Props> = (args) => <Toggler {...args} />;

export const ActiveExample = Template.bind({});
ActiveExample.args = {
  isActive: true,
  labelOff: 'Nej',
  labelOn: 'Ja',
};

export const InactiveExample = Template.bind({});
InactiveExample.args = {
  isActive: false,
  labelOff: 'Gör det nu',
  labelOn: 'Gör det imorgon',
};

export const InnerLabel = Template.bind({});
InnerLabel.args = {
  labelOff: "Gör det nu",
  labelOn: "Gör det imorgon",
  innerLabel: true
}
export const DisabledExample = Template.bind({});
DisabledExample.args = {
  isActive: true,
  disabled: true,
  labelOn: 'Kan inte ändra',
  labelOff: 'Något alls',
};

export const NoTextExample = Template.bind({});
NoTextExample.args = {
  isActive: true,
};


export const LargeExample = Template.bind({});
LargeExample.args = {
  size: 'large',
  labelOn: 'På',
  labelOff: 'Av',
};


export const ONOFFExample = Template.bind({});
ONOFFExample.args = {
  size: 'large',
  labelOn: 'ON',
  labelOff: 'OFF',
  innerLabel: true
};

export const ColorExample = Template.bind({});
ColorExample.args = {
  size: 'large',
  labelOn: 'Pink',
  labelOff: 'Aquamarine',
  colorOn: 'pink',
  colorOff: 'aquamarine'
}