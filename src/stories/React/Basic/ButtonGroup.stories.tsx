import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import { Button, ButtonGroup } from '../../../components/basic/Button';

export default {
  title: 'React/Basic/ButtonGroup',
  component: ButtonGroup
} as Meta;


const TemplateGroup: Story = (args) => <ButtonGroup {...args} />;

export const Group = TemplateGroup.bind({});
Group.args = {
  children: <><Button faIcon="fas fa-eye">First</Button><Button context="secondary" faIcon="fas fa-circle">Second</Button><Button context="info">Third</Button></>
}

const InlineGroup: Story = (args) => <div>Text before <ButtonGroup {...args} /> and text after</div>;

export const GroupInline = InlineGroup.bind({});
GroupInline.args = {
  children: <><Button faIcon="fas fa-eye">First</Button><Button context="secondary" faIcon="fas fa-circle">Second</Button><Button context="info">Third</Button></>
}