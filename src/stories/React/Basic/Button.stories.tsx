import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import { Button, ButtonProps } from '../../../components/basic/Button';

export default {
  title: 'React/Basic/Button',
  component: Button
} as Meta;

const Template: Story<ButtonProps> = (args) => <Button {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  context: 'primary',
  children: 'Primär knapp',
};

export const Secondary = Template.bind({});
Secondary.args = {
  context: 'secondary',
  children: 'Sekundär knapp',
};

export const Large = Template.bind({});
Large.args = {
  size: 'large',
  children: 'Stor knapp',
};

export const Small = Template.bind({});
Small.args = {
  size: 'small',
  children: 'Liten knapp',
};

export const Disabled = Template.bind({});
Disabled.args = {
  disabled: true,
  children: 'Avstängd',
};

export const Icon = Template.bind({});
Icon.args = {
  type: 'icon',
  faIcon: 'fas fa-volume-up'
}

export const Badge = Template.bind({});
Badge.args = {
  type: 'badge',
  faIcon: 'fas fa-pencil-alt'
}