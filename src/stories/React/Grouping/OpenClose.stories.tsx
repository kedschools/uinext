import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import { OpenCloseBox, Props } from '../../../components/grouping/OpenClose';

export default {
  title: 'React/Grouping/OpenCloseBox',
  component: OpenCloseBox
} as Meta;

const Template: Story<Props> = (args) => <OpenCloseBox {...args} />;


export const Default = Template.bind({});
Default.args = {
  title: 'Titel',
  children: 'Innehåll'
};


export const Warning = Template.bind({});
Warning.args = {
  className: 'warning',
  title: 'Titel',
  children: 'Innehåll'
}