import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import { Accordion, Props } from '../../../components/grouping/Accordion';

export default {
  title: 'React/Grouping/Accordion',
  component: Accordion
} as Meta;

const Template: Story<Props> = (args) => <Accordion {...args} />;


const sections =  [{
  title:  'Item 1',
  content: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eos, facilis.'
},
{
  title: 'Item 2',
  content: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nihil, aut.'
}]

export const Default = Template.bind({});
Default.args = {
  sections
};

export const Single = Template.bind({});
Single.args = {
  single: true,
  sections
};

export const Warning = Template.bind({});
Warning.args = {
  className: 'warning',
  sections
}