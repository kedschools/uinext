import '!style-loader!css-loader!sass-loader!../src/style/main.scss';
import { DocsContainer } from '../src/stories/DocContainer';

export const parameters = {
  docs: {
    container: DocsContainer,
  },
  actions: { argTypesRegex: '^on[A-Z].*' },
  setLocaleToKnob: true,
  themes: {
    default: 'kg',
    list: [
      { name: 'kg', class: 'theme-kg', color: '#00aced' },
      { name: 'ks', class: 'theme-ks', color: '#3b5998' },
    ],
  },
  darkMode: {
    stylePreview: true,
  },
};
