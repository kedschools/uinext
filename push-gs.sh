#!/bin/bash -e

GS_URL="gs://ked-public-web-files/content-files"

# Compile CSS
yarn compile-css

cp ./src/style/customisations/* dist/

echo "Publishing to $GS_URL"
gsutil -m rsync -x '(.*\/)*vendor\/test.*js.*' dist/ "$GS_URL"
#gsutil cors set "gsutil-cors.json" "$GS_URL"
#gcloud storage buckets update gs://ked-public-web-files/content-files --cors-file=gsutil-cors.json
